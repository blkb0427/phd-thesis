library(tidyverse)

dummy_tab <- data.frame(matrix(data = NA, nrow = 6100, ncol = 5))
colnames(dummy_tab) <- c("DrugName", "Sum_AB", "sum_sqrt_A2", "sum_sqrt_B2", "Cosine")
dummy_tab$DrugName <- colnames(dat_ori)[2:6101]
head(dummy_tab)
dim(dummy_tab)

# Load CMAP logFC ref db
#CMAP_dat_logFC <- read.delim("_CMAP_build02_FC.txt", header = TRUE, check.names = FALSE)
#CMAP_dat_logFC$GeneName <- rownames(CMAP_dat_logFC)
#CMAP_dat_logFC <- CMAP_dat_logFC[, c(6101, 1:6100)]


for (i in 2:6101) {
  temp <- CMAP_dat_logFC[, c(1, i)]
  ref_profile_sorted_decre_500 <- temp %>% top_n(500)
  ref_profile_sorted_ascend_500 <- temp %>% top_n(-500)
  ref_profile_sorted_1000 <- rbind(ref_profile_sorted_decre_500, ref_profile_sorted_ascend_500)
  
  #Subsetting UP genes, with FC
  up <- read.delim("DEG_query/Ushijima_datasets/XCos_up/C010_XCos_UP.txt", header = TRUE)
  up$GeneName <- as.character(up$GeneName)
  up.index <- which(ref_profile_sorted_1000$GeneName %in% up$GeneName)
  subset_up_ref_profile <- ref_profile_sorted_1000[up.index, ]
  
  #Subsetting DOWN genes, with FC
  down <- read.delim("DEG_query/Ushijima_datasets/XCos_down/C010_XCos_DN.txt", header = TRUE)
  down$GeneName <- as.character(down$GeneName)
  dn.index <- which(ref_profile_sorted_1000$GeneName %in% down$GeneName)
  subset_dn_ref_profile <- ref_profile_sorted_1000[dn.index, ]
  
  #Merge query up and down together
  query_up_subset_index <- which(up$GeneName %in% subset_up_ref_profile$GeneName)
  up_query_subset <- as.data.frame(up[query_up_subset_index, ])
  #colnames(up_query_subset) <- "GeneName"
  
  query_dn_subset_index <- which(down$GeneName %in% subset_dn_ref_profile$GeneName)
  dn_query_subset <- as.data.frame(down[query_dn_subset_index, ])
  #colnames(dn_query_subset) <- "GeneName"
  
  #This is my A
  query_up_dn_FC <- rbind(up_query_subset, dn_query_subset)
  query_up_dn_FC <- query_up_dn_FC[order(query_up_dn_FC$GeneName), ]
  #query_up_dn_FC <- as.data.frame(query_up_dn_FC)
  #colnames(query_up_dn_FC) <- "GeneName"
  #query_up_dn_FC$GeneName <- as.character(query_up_dn_FC$GeneName)
  
  #This is my B
  ref_up_dn_FC <- rbind(subset_up_ref_profile, subset_dn_ref_profile)
  ref_up_dn_FC <- ref_up_dn_FC[order(ref_up_dn_FC$GeneName), ]
  ref_up_dn_FC$GeneName <- as.character(ref_up_dn_FC$GeneName)
  
  #Merge
  FC_merge <- left_join(query_up_dn_FC, ref_up_dn_FC, by = "GeneName")
  
  #need to change the drug name
  FC_merge$AB <- FC_merge$logFC * FC_merge[, 3]
  
  FC_merge$A2 <- (FC_merge$logFC)^2
  FC_merge$B2 <- (FC_merge[, 3])^2
  sum_AB <- sum(FC_merge$AB)
  sum_sqrt_A2 <- sqrt(sum(FC_merge$A2))
  sum_sqrt_B2 <- sqrt(sum(FC_merge$B2))
  cosine <- sum_AB / (sum_sqrt_A2 * sum_sqrt_B2)
  dummy_tab[i-1, 2] <- sum_AB
  dummy_tab[i-1, 3] <- sum_sqrt_A2
  dummy_tab[i-1, 4] <- sum_sqrt_B2
  dummy_tab[i-1, 5] <- cosine
  
  print(i-1)
}

dummy_tab <- left_join(dummy_tab, cmap_name, by = "DrugName")
dummy_tab <- left_join(dummy_tab, drug_target, by = "cmap_name")
dummy_tab <- dummy_tab[order(dummy_tab$Cosine, decreasing = TRUE), ]
dummy_tab$Rank <- 1:nrow(dummy_tab)
head(dummy_tab)

write.csv(dummy_tab, "Results/Ushijima/XCos/C010_XCos_results.csv", 
          row.names = FALSE)
